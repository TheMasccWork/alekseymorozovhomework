/*
Напишите программу на Java, чтобы вывести каждый третий символ каждого второго слова.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FilesExTwo {
    public static void main(String[] args) throws IOException {
        String textContent = bufferedRead("input.txt");
        bufferedWrite("output.txt", textContent);
    }

    private static String bufferedRead(String str) throws IOException {
        Path pathForRead = Paths.get(str);
        Charset charset = StandardCharsets.UTF_8;
        String fileContent = "";
        try(BufferedReader reader = Files.newBufferedReader(pathForRead, charset)) {
            String s;
            while ((s = reader.readLine()) != null) {
                fileContent += s + "";
            }
        }
        return fileContent;
    }

    private static void bufferedWrite(String path, String content) throws IOException {
        Path pathForWrite = Paths.get(path);
        Charset charset = StandardCharsets.UTF_8;
        try(BufferedWriter writer = Files.newBufferedWriter(pathForWrite, charset)) {
            String strForWrite = "";
            String[] newStr = content.split(" ");
            for (int i = 1; i < newStr.length; i+=2) {
                String[] chars = newStr[i].split("");
                if (chars.length >= 3) {
                    strForWrite += chars[2].toUpperCase() + " ";
                }
            }
            writer.write(strForWrite);
        }
    }
}