/*
Реализовать челночную сортировку (Shuttle Sort).
 */

public class LessonTwoExFour {
    public static void main(String[] args) {
        int[] array = { 5, 10, 6, 4, 11 };
        for (int i = 1; i < array.length; i++) {
            if ( array[i] < array[i - 1] ) {
                int el1 = array[i - 1];
                array[i - 1] = array[i];
                array[i] = el1;
                for (int j = i - 1; (j - 1) >= 0; j--) {
                    if (array[j] < array[j - 1]) {
                        int el2 = array[j - 1];
                        array[j - 1] = array[j];
                        array[j] = el2;
                    } else {
                        break;
                    }
                }
            }
        }
        for (int j : array) {
            System.out.println(j);
        }
    }
}
