/*
Единицы массы пронумерованы следующим образом: 1 — килограмм,
2 — миллиграмм, 3 — грамм, 4 — тонна, 5 — центнер. Дан номер единицы массы
(целое число в диапазоне 1–5) и масса тела в этих единицах (вещественное число).
Найти массу тела в килограммах.
 */

import java.util.Scanner;

public class MethodsAndClassesExOne {
    public static void main(String[] args) {
        double result = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите данные:");
        int weightType = scanner.nextInt();
        double weight = scanner.nextDouble();
        switch (weightType) {
            case (1) -> result = weight;
            case (2) -> result = weight * 0.0000001;
            case (3) -> result = weight * 0.001;
            case (4) -> result = weight * 1000;
            case (5) -> result = weight * 100;
            default -> System.out.println("Вы ввели несуществующий тип массы");
        }
        System.out.println(result);
    }
}
