import java.util.Scanner;

/*
Вычислить значение функции y = f(x). Дана следующая функция y = f(x):
Пользователь вводит с клавиатуры значение переменной x.
Программа должна вывести  ответ  на консоль.

             y = 3x - 5, если x > 0
	         y = 0 , если x = 0
             y =  |x| - 1, если x < 0

Требуется найти значение функции по переданному.
 */

public class LessonOneExTwo {
    public static void main(String[] args) {
        int y;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Пожалуйста, введите X: ");
        int x = scanner.nextInt();
        if ( x > 0 ) {
            y = 3 * x - 5;
        } else if ( x < 0 ) {
            y = Math.abs(x) - 1;
        } else {
            y = 0;
        }
        System.out.println("Результат рассчета функции: " + y);
    }
}
