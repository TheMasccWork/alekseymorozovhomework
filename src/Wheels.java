public class Wheels {
    double diameter;
    String type;
    String firm;
    boolean deterioration;

    Wheels(double diameter, String type, String firm, boolean deterioration) {
        this.diameter = diameter;
        this.type = type;
        this.firm = firm;
        this.deterioration = deterioration;
    }

    public void drift() {
        System.out.println("Идем в занос");
    }
}
