package GenericsExTwo;

import java.math.BigInteger;
import java.util.Scanner;

public class GenericsExTwo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CalcSum<BigInteger> calc = new CalcSum<BigInteger>();
        BigInteger one = new BigInteger(scanner.next());
        BigInteger two = new BigInteger(scanner.next());
        BigInteger total = calc.sumValue(one, two, BigInteger::add);
        System.out.println(total);
    }
}
