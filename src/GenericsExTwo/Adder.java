package GenericsExTwo;

interface Adder<T extends Number> {
    T add(T lhs, T rhs);
}
