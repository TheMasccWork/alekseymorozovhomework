/*
Дан текст, в котором имеются цифры.
а) Найти их сумму.
б) Найти максимальную цифру.
 */

import java.util.ArrayList;

public class StringsExTwo {
    public static void main(String[] args) {
        textConvert("Это 100 был 20 не 999 я!");
    }

    static void textConvert(String text) {
        int sum = 0;
        int max = 0;
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        String[] textArr = text.split(" ");
        for (String a: textArr) {
            try {
                int i = Integer.parseInt(a);
                numbers.add(i);
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
        }
        for (Integer number : numbers) {
            sum += number;
            max = number > max ? number : max;
        }
        System.out.println("Сумма чисел в переданном тексте = " + sum);
        System.out.println("Максимальное число = " + max);
    }
}
