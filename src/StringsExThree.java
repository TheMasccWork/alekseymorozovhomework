/*
Даны два слова. Напечатать только те буквы слов, которые есть
только в одном из них (в том числе повторяющиеся). Например, если
заданные слова процессор и информация, то ответом должно быть: п е с с и ф м а я.
 */
import java.util.ArrayList;

public class StringsExThree {
    public static void main(String[] args) {
        uniqueChars("Алексей", "Вениамин");
    }

    static void uniqueChars(String str1, String str2) {
        String[] firstStrArray = str1.split("");
        String[] secondStrArray = str2.split("");
        ArrayList<String> newString = new ArrayList<>();
        int a = 0;
        for (String i: firstStrArray) {
            for (String j: secondStrArray) {
                if (i.equalsIgnoreCase(j)) {
                    a += 1;
                }
            }
            if ( a == 0) {
                newString.add(i.toLowerCase());
            }
            a = 0;
        }
        for (String i: secondStrArray) {
            for (String j: firstStrArray) {
                if (i.equalsIgnoreCase(j)) {
                    a += 1;
                }
            }
            if ( a == 0) {
                newString.add(i.toLowerCase());
            }
            a = 0;
        }
        if (newString.size() == 0) {
            System.out.println("Уникальных символов не обнаружено!");
        } else {
            System.out.println("Полученный массив уникальных символов: " + newString.toString());
        }
    }
}
