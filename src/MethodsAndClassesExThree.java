/*
Даны цифры двух челых чисел: двузначного a1a2 и однозначного b,
где а1 - число единиц, а2 - число десятков. Получить цифры числа,
равного разности заданных чисел (известно, что это число двузначное).
Число-уменьшаемое и число-разность не определять.
 */

public class MethodsAndClassesExThree {
    public static void main(String[] args) {
        method(0,0,3);
    }
    public static void method(int a1, int a2, int b) {
        int c1;
        int c2;
        if ((a1 >= 0 && a1 < 10) && (a2 > 0 && a2 < 10) && ( (a2 * 10 + a1 - b) >= 10 )) {
            if( a1 < b ) {
                c2 = a2 - 1;
                c1 = a1 + 10 - b;
            } else {
                c2 = a2;
                c1 = a1 - b;
            }
            System.out.println("Число десятков в результирующем числе: " + c2 + ", число единиц: " + c1);
        } else {
            System.out.println("Вы ввели неправильные числа!");
        }
    }
}
