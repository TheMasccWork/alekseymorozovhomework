public class Example {
    public static void main(String[] args) {
        Engine engine = new Engine(150, 3, 2000);
        Wheels wheels1 = new Wheels(17, "highway", "Continental", false );
        Wheels wheels2 = new Wheels(17, "highway", "Continental", false );
        Wheels wheels3 = new Wheels(17, "highway", "Continental", false );
        Wheels wheels4 = new Wheels(17, "highway", "Continental", false );
        Wheels[] wheels = {wheels1, wheels2, wheels3, wheels4};
        int speed = 240;
        String brand = "BMW";


        Car car = new Car(engine, wheels, brand, speed);
    }
}
