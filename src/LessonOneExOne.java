import java.util.Scanner;

/*
Пользователю необходимо ввести число с клавиатуры. Если оно является положительным,
то прибавить к нему 2; в противном случае умножить на 2. Вывести полученное число.
 */

public class LessonOneExOne {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Пожалуйста, введите число для проверки: ");
        int i = scanner.nextInt();
        if ( i >= 0 ) {
            i = i + 2;
            System.out.println("Введенное вам число - положительное. Результат:" + i);
        } else {
            i = i * 2;
            System.out.println("Введеннео вами число >= 0. Результат: " + i);
        }
    }
}
