/*
Дано вещественное число A и целое число N (> 0). Используя один цикл,
найти значение выражения 1 – A + A*2 – A*3 + … + (–1)N * AN.
Условный оператор не использовать.
 */

public class LessonTwoExTwo {
    public static void main(String[] args) {
        float A = 7.99F;
        int N = 4;
        float summary = 1;
        for (int i = 1; i < N + 1; i++) {
            summary += A * i* Math.pow(-1, i);
            System.out.println("Итерация №" + i + ". Суммарное значение на этой итерации = " + summary);
        }
        System.out.println("Итоговое значение = " + summary);
    }
}
