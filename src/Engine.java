public class Engine {
    int power;
    int pistons;
    double weight;

    Engine(int power, int pistons, double weight) {
        this.power = power;
        this.pistons = pistons;
        this.weight = weight;
    }

    public void start() {
        System.out.println("Старт!");
    }
    public void beep() {
        System.out.println("Бип-бип");
    }
}
