/*
Алиса решила купить новую скатерть. Перед тем, как отправиться в магазин,
она измерила ширину и длину поверхности стола и рассчитала его периметр.
Однако к тому времени, как она добралась до магазина, она вспомнила только
периметр. Напишите программу на Java, которая для заданного периметра
восстанавливает все возможные размеры поверхности стола. Для полных точек
следует определить и использовать отдельный метод, который генерирует все
пары измерений, в то время как ввод и вывод программы должны по-прежнему
выполняться основным методом.
 */

import java.util.*;

public class ExceptionsExThree {
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            int perimetr = scanner.nextInt();
            List<Integer[]> pairs = takeAllPairs(perimetr);
            for (Integer[] pair : pairs) {
                System.out.println(Arrays.toString(pair));
            }
        } catch (InputMismatchException e) {
            System.out.println("Значение периметра может быть только целым числом!");
        } catch (InvalidInputParamExeption e) {
            e.printStackTrace();
        }
    }

    // Для упрощение рассчетов решил сделать так, что периметр должен быть четным числом больше 4.
    // Потому что если взять нечетное число, то длины сторон будут вещественными числами,
    // и тогда я без понятия до какой точности считать. В таком случае можно даже из 15 метров
    // сделать огромнейшее количество пар. Но если потребуется, могу переделать рассчеты - это
    // вопрос пары лишних строк кода. Только тогда мне нужны уточнения.

    private static List<Integer[]> takeAllPairs(int perimetr) {
        if (perimetr >= 4 && perimetr % 2 == 0) {
            List<Integer[]> list = new ArrayList<>();
            for (int i = 2; i < perimetr; i += 2) {
                if ((perimetr - i) / 2 != 0) {
                    list.add(new Integer[]{i / 2, (perimetr - i) / 2});
                }
            }
            return list;
        } else {
            throw new InvalidInputParamExeption("Вы указали слишком маленькое или нечетное число");
        }
    }

    public static class InvalidInputParamExeption extends RuntimeException {
        public InvalidInputParamExeption(String message) {
            super(message);
        }
    }
}