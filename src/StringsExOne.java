/*
Дано слово. Получить и вывести на экран буквосочетание, состоящее из его
третьего и последнего символа.
 */

public class StringsExOne {
    public static void main(String[] args) {
        newString("Россия");
    }

    static String newString(String str) {
        String newStr = null;
        if (str.length() < 4) {
            System.out.println("Вы ввели слишком короткое слово");
        } else {
            String charFirst = Character.toString(str.charAt(2));
            String charLast = Character.toString(str.charAt(str.length()-1));
            newStr = charFirst + charLast;
        }
        System.out.println(newStr);
        return newStr;
    }
}
