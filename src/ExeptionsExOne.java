/*
Написать примеры  программ в которых могут возникнуть проверяемые и непроверяемые исключения, обработать их непосредственно в методе а также в месте вызова метода.
 */

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ExeptionsExOne {
    public static void main(String[] args) {
        try {
            exeptionMethod();
        } catch (IOException e) {
            System.out.println("Например, проблемы с чтением файла");
        } catch (NullPointerException e) {
            System.out.println("Нельзя закрыть то, что не открыто");
        }

    }

    static void exeptionMethod() throws IOException, NullPointerException{
        PrintWriter writer = null;
        try {
            List<Integer> list = new ArrayList<>();
            Scanner scanner = new Scanner(System.in);
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            list.add(a);
            list.add(b);
            System.out.println(list.get(1)/list.get(0));
            writer = new PrintWriter(new FileWriter("out.txt"));
            writer.println("Something");
        } catch (ArithmeticException e) {
            System.out.println("На ноль делить нельзя!");
        } finally {
            writer.close();
        }

    }
}