/*
Напишите программу на Java, чтобы проверить, существует файл или каталог, указанный в pathname, или нет.
 */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FilesExOne {
    public static void main(String[] args) throws IOException {
        checkPath("input.txt");
    }

    public static void checkPath(String pathname) {
        Path path = Paths.get(pathname);
        System.out.println(Files.exists(path));
    }
}
