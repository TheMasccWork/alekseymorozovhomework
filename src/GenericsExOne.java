/*
Напишите простую универсальную версию метода isEqualTo, которая сравнивает два его
аргумента с методом equals и возвращает true, если они равны, и false в противном случае.
Используйте этот универсальный метод в программе, которая вызывает isEqualTo с различными
встроенными типами, такие как объект или целое число. Какой результат вы получите при попытке
запустить эту программу?
 */

public class GenericsExOne {
    public static void main(String[] args) {
        String str1 = new String("null");
        String str2 = null;
        Integer num1 = 10;
        int num2 = 10;
        System.out.println(isEqualTo(str1, str2));
        System.out.println(isEqualTo(num1, num2));
        System.out.println(isEqualTo(str1, num1));
    }

    public static <T> boolean isEqualTo(T arg1, T arg2) throws NullPointerException{
        if (arg1 == null || arg2 == null) {
            return arg1==arg2;
        } else {
            return arg1.equals(arg2);
        }
    }
}
