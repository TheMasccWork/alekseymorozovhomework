import java.util.Scanner;

/*
Написать  программу, которая вводит координаты точки (x, y) и определяет,
попадает ли точка в заштрихованную область на рисунке, который соответствует
Вашему варианту. Попадание на границу области считать попаданием в область.
Ввод координат осуществлять вручную с помощью клавиатуры.
 */

public class LessonOneExThree {
    public static void main(String[] args) {
        int r = 1;
        int x0 = 0;
        int y0 = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Пожалуйста, введите координату точки Х:");
        float x = scanner.nextFloat();
        System.out.println("Пожалуйста, введите координату точки Y:");
        float y = scanner.nextFloat();
        System.out.println("Вы ввели координаты (" + x + "," + y + "). Рассчет по ним: ");
        if ( (x > 0 && y > 0) || ( x < 0 && y < 0 ) ) {
            System.out.println("Точка не входит в заданную область");
        } else {
            boolean i = Math.pow((x - x0), 2) + Math.pow((y - y0), 2) <= Math.pow(r, 2);
            if (i) {
                System.out.println("Точка в координатами (" + x + "," + y + ") входит в заданную область");
            } else {
                System.out.println("Точка в координатами (" + x + "," + y + ") НЕ входит в заданную область");
            }
        }
    }
}
